(function() {
  'use strict';

  function Game() {
    this.player = null;
    this.$ = window.$;
    // How to do this without window?
    this.gameData = window.gameData;
  }

  Game.prototype = {

    create: function () {

        this.text = [];
        this.things = [];
        this.world.setBounds(0, 0, 2054, 2164);
        this.add.sprite(0, 0, 'water');
        this.add.sprite(500, 500, 'backdrop');
        this.card = this.add.sprite(1000, 1000, 'hero');
        this.physics.enable(this.card, Phaser.Physics.ARCADE);
        this.card.body.collideWorldBounds = true;
        this.camera.follow(this.card);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.jumpButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        if (!window.positions2) {
            var data = window.localStorage["game"];
            this.placements = JSON.parse(data);
        } else {
            this.placements = JSON.parse(window.positions2);
        }
        this.objects = []

        console.log(data);

        for(var i=0; i < this.placements.length; i++) {
            var placement = this.placements[i];
            var position = placement.position;
//            this.thing = 
            var thing = this.add.sprite(position.x, position.y, placement.name);
            this.things.push(placement);
            this.objects.push(thing);
            this.physics.enable(thing, Phaser.Physics.ARCADE);
            thing.body.immovable = true;
        }

            //back.fixedToCamera = true;
            //

        $("#title").text(window.gameData.configuration.name);
        $("#description").text(window.gameData.configuration.description);

    },

    update: function () {

        for(var i=0; i<this.text.length; i++) {
            this.text[i].destroy();
        }
        this.text = [];
        var _this = this;

        this.camera.follow(this.card);
        for(var j=0; j<this.objects.length; j++) {
            this.physics.arcade.collide(this.card, this.objects[j], function(x, y) {
                //var str = 
                var style = { font: "18px Arial", fill: "#00000", align: "center" };


                _this.camera.unfollow();
                _this.camera.focusOn(_this.objects[j]);
               //_this.world.x = l500, 400); 
                
                var c = _this.add.sprite(_this.objects[j].x, _this.objects[j].y - 300 , 'speach');
    //            c.fixedToCamera = true;
                _this.text.push(c);
                console.log(">>>");
                console.log(j);
                console.log(_this.things[j]);
                var str = _this.gameData.actors.filter(function(x) { return _this.things[j].name == x.name;
                })[0].dialog;
                console.log(_this.gameData.actors);
                console.log(_this.things);
                if (typeof(str) == "string") {
                    str = [str];
                };
                for(var i=0; i<str.length; i++) {
                    var t = _this.add.text(_this.objects[j].x + 30, _this.objects[j].y - 230 + i * 20, str[i], style);
                    
     //               t.fixedToCamera = true;
                    _this.text.push(t);
                };

                //_this.add.geom(back,'#cfffff');
                //
            });
        };



        
        this.card.body.velocity.y = 0;
        this.card.body.velocity.x = 0;

        if (this.cursors.left.isDown)
        {
            this.card.body.velocity.x -= 200;
        }
        else if (this.cursors.right.isDown)
        {
            this.card.body.velocity.x += 200;
        }

        if (this.cursors.up.isDown)
        {
            this.card.body.velocity.y -= 200;
        }
        else if (this.cursors.down.isDown)
        {
            this.card.body.velocity.y += 200;
        }

        if (this.jumpButton.isDown) {
            console.log("SPACE BAR");
        }
    }

  };

  window['newsgame'] = window['newsgame'] || {};
  window['newsgame'].Game = Game;

}());
