window.onload = function () {
  'use strict';

  var game
    , ns = window['newsgame']
    , $ = window.$;

  var getSetup = $.getJSON('gamesetup.json');

  getSetup.done(function(setup) {
      // Not ideal.
      window.gameData = setup;

      game = new Phaser.Game(900, 600, Phaser.AUTO, 'newsgame-game');
      game.state.add('boot', ns.Boot);
      game.state.add('preloader', ns.Preloader);
      game.state.add('menu', ns.Menu);
      game.state.add('game', ns.Game);
      game.state.start('preloader');
  });

  $("#play").click(function() {
      console.log("PLAY!");
      window.location = "/play.html";
  })
  $("#clear").click(function() {
      console.log("clear!")
  })

  getSetup.fail(function(a) {
      console.log("error loading json");
  });

};
