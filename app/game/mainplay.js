window.onload = function () {
  'use strict';

  var game
    , ns = window['newsgame']
    , $ = window.$;


  var setup = JSON.parse(window.localStorage["gameData"]);
  window.gameData = setup

  game = new Phaser.Game(1100, 600, Phaser.AUTO, 'newsgame-game');
  game.state.add('preloader', ns.Preloader);
  game.state.add('game', ns.Game);
  game.state.start('preloader');


};
