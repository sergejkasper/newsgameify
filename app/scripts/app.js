'use strict';

angular
  .module('newsgamifyApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angularFileUpload',
    'angular-loading-bar'
  ])
  .config(function ($routeProvider, cfpLoadingBarProvider) {
     cfpLoadingBarProvider.latencyThreshold = 3;
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/export', {
        templateUrl: 'views/export.html',
        controller: 'ExportCtrl'
      })
      .when('/actors', {
        templateUrl: 'views/actors.html',
        controller: 'ActorsCtrl'
      })
      .when('/map', {
        templateUrl: 'views/map.html',
        controller: 'MapCtrl'
      })
      .when('/game', {
        templateUrl: 'views/game.html',
        controller: 'GameCtrl'
      })
      .when('/uns', {
        templateUrl: 'views/uns.html',
        controller: 'UnsCtrl'
      })
      .when('/kontakt', {
        templateUrl: 'views/kontakt.html',
        controller: 'KontaktCtrl'
      })
      .when('/navbar', {
        templateUrl: 'views/navbar.html',
        controller: 'NavbarCtrl'
      })
      .when('/play', {
        templateUrl: 'views/play.html',
        controller: 'PlayCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function ($rootScope) {
    if (!sessionStorage.actors) {
      sessionStorage.actors = [];
      console.log('created: ' + angular.toJson(sessionStorage.actors, true));
    }
    if (!sessionStorage.map) {
      sessionStorage.map = {};
      console.log('created: ' + angular.toJson(sessionStorage.map, true));
    }
    if (!sessionStorage.meta) {
      sessionStorage.meta = {};
      console.log('created: ' + angular.toJson(sessionStorage.meta, true));
    }
  });
