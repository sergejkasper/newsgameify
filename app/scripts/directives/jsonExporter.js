'use strict';

angular.module('newsgamifyApp').directive('myDownload', function($compile) {
  return {
    restrict: 'E',
    scope: {
      getUrlData: '&getData'
    },
    link: function(scope, elm) {
      var url = URL.createObjectURL(scope.getUrlData());
      elm.append($compile(
        '<a style="color:white" download="konfiguration.json"' +
        'href="' + url + '">' +
        ' Konfigurationsdatei herunterladen' +
        '</a>'
      )(scope));
    }
  };
});