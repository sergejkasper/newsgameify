'use strict';

/**
 * @ngdoc directive
 * @name newsgamifyApp.directive:gamemap
 * @description
 * # gamemap
 */
angular.module('newsgamifyApp')
  .directive('gamemap', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element) {
        element.text('this is the gamemap directive');
      }
    };
  });
