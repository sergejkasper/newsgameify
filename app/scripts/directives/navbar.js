'use strict';

/**
 * @ngdoc directive
 * @name newsgamifyApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('newsgamifyApp')
  .directive('navBar', function () {
    return {
     scope: {},  // use a new isolated scope
    restrict: 'E',
      templateUrl: '/views/navbar.html'
    };
  });
