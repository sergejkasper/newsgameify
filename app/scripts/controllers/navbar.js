'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
  });
