'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:PlayCtrl
 * @description
 * # PlayCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('PlayCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
