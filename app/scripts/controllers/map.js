'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:MapCtrl
 * @description
 * # MapCtrl
 * Controller of the newsgamifyApp
 
 {
  'configuration': {
    'name': '',
    'description': '',
    'topic': '',
    'tutorial': 'markup',
    'img': 'http://www.google.de/img'
  },
  'hero' : {
    'name': 'Hero',
    'img': ''
  },
  'actors': [
  ]
}


 */





angular.module('newsgamifyApp')
  .controller('MapCtrl', function ($scope, $location) {
    var setup = {
      'hero' : {
        'name': 'Hero',
        'image': '/assets/images/ViviVolo.png'
      },
      'configuration': JSON.parse(localStorage.gamedescription),
      'actors': JSON.parse(sessionStorage.actors) 
    };
    //setup.configuration.image = '/assets/images/africa.png';
    //setup.push(JSON.parse(localStorage.gamedescription));
    var game = [];
    window.gameData = setup;
    // if (window.game) {
    //   game.destroy();
    // }
    var game
      , ns = window['newsgame']
      , $ = window.$,
      Phaser = window.Phaser,
    game = new Phaser.Game(650, 500, Phaser.AUTO, 'newsgame-game');
    //game.state.add('boot', ns.Boot);
    game.state.add('preloader', ns.Preloader);
    game.state.add('menu', ns.Menu);
    game.state.add('game', ns.Game);
    game.state.start('preloader');

    window.game = game;
    $scope.save = function save() {
      localStorage.map = {};
      //localStorage.map = angular.toJson($scope.meta);
      //localStorage.gamedata = angular.toJson(game);
      $location.path('/export')
    };
  });
