'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:ExportCtrl
 * @description
 * # ExportCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('ExportCtrl', function($scope) {
    $scope.configuration = {
      name: '',
      description: '',
      topic: '',
      tutorial: '',
      img: ''
    };
    $scope.hero = {
      name: '',
      img: ''
    };
    $scope.actors = [];
    var data = {};
    //data.push(localStorage.actors);
	//data.push(localStorage.map);
    //data.push(localStorage.gamedescription);

    var json = JSON.stringify(data);

    $scope.getBlob = function() {
      return new Blob([json], {
        type: 'application/json'
      });
    };
  });