'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:ActorsCtrl
 * @description
 * # ActorsCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('ActorsCtrl', function($scope, FileUploader, $location) {
    $scope.uploader = new FileUploader();
    $scope.actors = [{
      name: 'Name',
      description: '',
      dialog: [],
      image: '',
      position: {
        x: '',
        y: ''
      }
    }];

    $scope.selector = {
    'actors': [
        {
            'image': '/assets/images/Airport.png',
            'name': 'Airport'
        },
        {
            'image': '/assets/images/Birds.png',
            'name': 'Birds'
        },
        {
            'image': '/assets/images/Nurse.png',
            'name': 'Nurse'
        },
        {
            'image': '/assets/images/Palmtree.png',
            'name': 'Palmtree'
        },
        {
            'image': '/assets/images/Palmtree2.png',
            'name': 'Palmtree2'
        },
        {
            'image': '/assets/images/RedCrossTent.png',
            'name': 'RedCrossTent'
        },
        {
            'image': '/assets/images/ScientistOnly.png',
            'name': 'Scientist'
        },
        {
            'image': '/assets/images/Soldier.png',
            'name': 'Soldier'
        },
        {
            'image': '/assets/images/UN-Truck.png',
            'name': 'UN-Truck'
        },
        {
            'image': '/assets/images/UN-Truck2.png',
            'name': 'UN-Truck2'
        },
        {
            'image': '/assets/images/Village.png',
            'name': 'Village'
        },
        {
            'image': '/assets/images/Village2.png',
            'name': 'Village2'
        },
        {
            'image': '/assets/images/VillageAlone.png',
            'name': 'VillageAlone'
        },
        {
            'image': '/assets/images/Virus.png',
            'name': 'Virus'
        },
        {
            'image': '/assets/images/ViviVolo.png',
            'name': 'ViviVolo'
        }
    ]};


    $scope.addActor = function() {
      $scope.actors.push({
        name: 'Name',
        description: '',
        dialog: [],
        image: '',
        position: {
          x: '',
          y: ''
        }
      });
    };

    $scope.removeActor = function(indx) {
      $scope.actors.splice(indx, 1);
    };

    $scope.saveActors = function() {
      sessionStorage.actors = [];
      sessionStorage.actors = angular.toJson($scope.actors);
      localStorage.actors = [];
      localStorage.actors = angular.toJson($scope.actors);
      console.log(sessionStorage.actors);
      $location.path('/map');
    };
  });
