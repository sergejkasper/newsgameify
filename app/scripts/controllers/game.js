'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:GameCtrl
 * @description
 * # GameCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('GameCtrl', function($scope, $rootScope, $location) {
    $scope.image = {
      img: [{
        img: '/assets/images/africa.png',
        name: 'Afrika'
      }]
    };
    $scope.meta = {};
    $scope.save = function save() {
      localStorage.gamedescription = [];
      localStorage.gamedescription = angular.toJson($scope.meta);
      console.log(localStorage.gamedescription);
      $location.path('/actors');
    };
  });


// "configuration": {
//     "image": "/assets/images/africa.png",
//     "name": "Africa",
// },