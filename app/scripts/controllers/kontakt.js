'use strict';

/**
 * @ngdoc function
 * @name newsgamifyApp.controller:KontaktCtrl
 * @description
 * # KontaktCtrl
 * Controller of the newsgamifyApp
 */
angular.module('newsgamifyApp')
  .controller('KontaktCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
