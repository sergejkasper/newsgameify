(function() {
  'use strict';

  function Preloader() {
    this.asset = null;
    this.ready = false;
    // How to do this without window?
    this.gameData = window.gameData;
  }

  Preloader.prototype = {

    preload: function () {
      this.asset = this.add.sprite(320, 240, 'preloader');
      this.asset.anchor.setTo(0.5, 0.5);
      this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
      this.load.setPreloadSprite(this.asset);
      this.load.image('hero', gameData.hero.image);
      this.load.bitmapFont('minecraftia', 'assets/minecraftia.png', 'assets/minecraftia.xml');
      this.load.image('backdrop', gameData.configuration.image);
      this.load.image('card', 'assets/sprites/mana_card.png');

      for(var i=0; i < gameData.actors.length; i++) {
          var actor = gameData.actors[i]
          console.log("LOADING:", actor.name);
          this.load.image(actor.name, actor.image);
      };

    },

    create: function () {
      this.asset.cropEnabled = false;
    },

    update: function () {
      if (!!this.ready) {
        this.game.state.start('game');
      }
    },

    onLoadComplete: function () {
      this.ready = true;
    }
  };

  window['newsgame'] = window['newsgame'] || {};
  window['newsgame'].Preloader = Preloader;

}());
