(function() {
  'use strict';

  function Game() {
    this.player = null;
    this.$ = window.$;
    // How to do this without window?
    this.gameData = window.gameData;
  }

  Game.prototype = {

    create: function () {
        this.world.setBounds(0, 0, 1054, 1164);
        this.add.sprite(0, 0, 'backdrop');
        this.card = this.add.sprite(200, 200, 'hero');
        this.physics.enable(this.card, Phaser.Physics.ARCADE);
        this.card.body.collideWorldBounds = true;
        this.camera.follow(this.card);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.jumpButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.items = [];

        for(var i=0; i < gameData.actors.length; i++) {
            var actor = gameData.actors[i]
            this.$('#item-type').append($('<option/>').attr({ 'value': actor.name }).text(actor.name))
        };
    },

    update: function () {

        if (this.cursors.left.isDown)
        {
            this.card.x -= 4;
        }
        else if (this.cursors.right.isDown)
        {
            this.card.x += 4;
        }

        if (this.cursors.up.isDown)
        {
            this.card.y -= 4;
        }
        else if (this.cursors.down.isDown)
        {
            this.card.y += 4;
        }

        if (this.jumpButton.isDown) {
           var selected = this.$('#item-type').val();
           console.log(selected);
           var thing = this.add.sprite(this.card.x, this.card.y, selected);
           this.items.push({
               'position': {
                   'x': this.card.x,
                   'y': this.card.y
               },
           //    'item': thing,
               'name': selected
           });
           console.log("SAVE");
           console.log(this.items);
           var json = JSON.stringify(this.items);
           window.localStorage["game"] = json;
        }
    }

  };

  window['newsgame'] = window['newsgame'] || {};
  window['newsgame'].Game = Game;

}());
