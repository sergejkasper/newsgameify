(function() {
  'use strict';

  function Game() {
    this.player = null;
    this.$ = window.$;
    // How to do this without window?
    this.gameData = window.gameData;
  }

  Game.prototype = {

    create: function () {
        this.text = [];
        this.world.setBounds(0, 0, 1054, 1164);
        this.add.sprite(0, 0, 'backdrop');
        this.card = this.add.sprite(200, 200, 'hero');
        this.physics.enable(this.card, Phaser.Physics.ARCADE);
        this.card.body.collideWorldBounds = true;
        this.camera.follow(this.card);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.jumpButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        var data = window.localStorage["game"];
        this.placements = JSON.parse(data);
        console.log(this.placements);
        this.objects = []

        for(var i=0; i < this.placements.length; i++) {
            var placement = this.placements[i];
            var position = placement.position;
            var thing = this.add.sprite(position.x, position.y, placement.name);
            this.objects.push(thing);
            this.physics.enable(thing, Phaser.Physics.ARCADE);
            thing.body.immovable = true;
        }


    },

    update: function () {

        for(var i=0; i<this.text.length; i++) {
            this.text[i].destroy();
        }
        var _this = this;
        this.physics.arcade.collide(this.card, this.objects, function() {
            var str = [
            "Einige Menschen stehen uns skeptisch gegenüber.", 
            "Die Existenz der Krankheit wird nicht wahrgenommen,", 
            "sie wird totgeschwiegen.",
            "Das trägt zur weiteren Ausbreitung und vielen weiteren",
            "Opfern bei.", 
            "Zum Beispiel, wenn die Menschen ihre Toten traditionell", 
            "waschen.",
            "Ebola kann nur durch direkten Körperkontakt übertragen",
            "werden - und dort geschieht das."]
            var style = { font: "20px Lucidatypewriter, monospace", fill: "#00000", align: "center" };
            for(var i=0; i<str.length; i++) {
                var t = _this.add.text(100, 100 + i * 20, str[i], style);
                t.fixedToCamera = true;
                _this.text.push(t);
            }
        });



        
        this.card.body.velocity.y = 0;
        this.card.body.velocity.x = 0;

        if (this.cursors.left.isDown)
        {
            this.card.body.velocity.x -= 200;
        }
        else if (this.cursors.right.isDown)
        {
            this.card.body.velocity.x += 200;
        }

        if (this.cursors.up.isDown)
        {
            this.card.body.velocity.y -= 200;
        }
        else if (this.cursors.down.isDown)
        {
            this.card.body.velocity.y += 200;
        }

        if (this.jumpButton.isDown) {
            console.log("SPACE BAR");
        }
    }

  };

  window['newsgame'] = window['newsgame'] || {};
  window['newsgame'].Game = Game;

}());
