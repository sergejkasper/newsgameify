'use strict';

describe('Controller: UnsCtrl', function () {

  // load the controller's module
  beforeEach(module('newsgamifyApp'));

  var UnsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UnsCtrl = $controller('UnsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
