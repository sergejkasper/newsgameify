'use strict';

describe('Controller: KontaktCtrl', function () {

  // load the controller's module
  beforeEach(module('newsgamifyApp'));

  var KontaktCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KontaktCtrl = $controller('KontaktCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
