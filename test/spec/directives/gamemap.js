'use strict';

describe('Directive: gamemap', function () {

  // load the directive's module
  beforeEach(module('newsgamifyApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<gamemap></gamemap>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the gamemap directive');
  }));
});
